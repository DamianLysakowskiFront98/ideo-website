document.addEventListener("DOMContentLoaded", () => {
  const searchIconEl = document.getElementById("search-icon");
  const sideNavArrowEl = document.getElementById("side-nav-arrow");

  const handleSideNavArrowClicked = () => {
    const sideNavEl = document.getElementById("side-nav");
    const sideNavListEl = document.getElementById("side-nav-list");

    sideNavListEl.classList.toggle(
      "side-nav__navigation-list-container--closed"
    );
    sideNavEl.classList.toggle("side-nav--closed");
    sideNavArrowEl.classList.toggle("side-nav__arrow--open");
  };

  const handleSearchIconClicked = () => {
    const inputSearchEl = document.getElementById("header__input");

    inputSearchEl.classList.toggle("visibility--visible");
  };

  searchIconEl.addEventListener("click", handleSearchIconClicked);
  sideNavArrowEl.addEventListener("click", handleSideNavArrowClicked);
});

// contact-slider

const slidesMobile = document.querySelectorAll(".item-mobile");
const buttonMobile = document.querySelectorAll(".button-mobile");

let currentMobile = 0;
let prevMobile = 2;
let nextMobile = 1;

for (let i = 0; i < buttonMobile.length; i++) {
  buttonMobile[i].addEventListener("click", () =>
    i === 0 ? gotoPrevMobile() : gotoNextMobile()
  );
}

const gotoPrevMobile = () =>
  currentMobile > 0
    ? gotoNumMobile(currentMobile - 1)
    : gotoNumMobile(slidesMobile.length - 1);

const gotoNextMobile = () =>
  currentMobile < 2 ? gotoNumMobile(currentMobile + 1) : gotoNumMobile(0);

const gotoNumMobile = (number) => {
  currentMobile = number;
  prevMobile = currentMobile - 1;
  nextMobile = currentMobile + 1;

  for (let i = 0; i < slidesMobile.length; i++) {
    slidesMobile[i].classList.remove("active-mobile");
    slidesMobile[i].classList.remove("prev-mobile");
    slidesMobile[i].classList.remove("next-mobile");
  }

  if (nextMobile === 3) {
    nextMobile = 0;
  }

  if (prevMobile === -1) {
    prevMobile = 2;
  }

  slidesMobile[currentMobile].classList.add("active-mobile");
  slidesMobile[prevMobile].classList.add("prev-mobile");
  slidesMobile[nextMobile].classList.add("next-mobile");
};

// slider footer

const gap = 16;

const carousel = document.getElementById("carousel"),
  content = document.getElementById("content"),
  testone = document.getElementById("inerwidth"),
  nexts = document.getElementById("nexts"),
  prevs = document.getElementById("prevs");

nexts.addEventListener("click", (e) => {
  carousel.scrollBy(width + gap, 0);
  if (carousel.scrollWidth !== 0) {
    prevs.style.display = "flex";
  }
});
prevs.addEventListener("click", (e) => {
  carousel.scrollBy(-(width + gap), 0);
  if (!testone.scrollWidth - width - gap <= carousel.scrollLeft + width) {
    nexts.style.display = "flex";
  }
});

let width = testone.offsetWidth;
window.addEventListener("resize", (e) => (width = carousel.offsetWidth));
